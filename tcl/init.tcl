#!/usr/bin/env tclsh

set ::WIN {}

proc main {} {
	if {![info exists ::env(SERVER_URL)]} {
		puts -nonewline stdout "Please enter the server URL (http://localhost:8000): "
		flush stdout
		gets stdin ::SERVER
	} else {
		set ::SERVER "$::env(SERVER_URL)"
	}

	if {[string length $::SERVER] == 0} {
		set ::SERVER {http://localhost:8000}
	}

	set pkgs {tls Tk base64 tkcon Img http huddle json}
	foreach pkg $pkgs {
		set pkg_version [package require $pkg]
		puts "\[i\] $pkg v$pkg_version"
	}

	set ::initial_scaling [tk scaling]

	http::register https 443 tls::socket

	bind . <Control-KeyRelease> {
		if {"%K" == "r"} {
			load_window 1
		}
	}

	load_window
}

proc load_window {{is_reload 0}} {
	source utils.tcl
	clean_win
	set w .
	set width [utils::scaleup 600]
	set height [utils::scaleup 400]
	set x [expr { ( [winfo vrootwidth  $w] - $width  ) / 2 }]
	set y [expr { ( [winfo vrootheight $w] - $height ) / 2 }]
	wm geometry $w ${width}x${height}+${x}+${y}
	update

	source login.tcl
	source ripple.tcl

	if {$is_reload} {
		puts "Source code reloaded"
	}

	paint
}

proc clean_win {} {
	foreach item $::WIN {
		catch {destroy $item}
	}
	set ::WIN {}
}

## Init
if {[string match $tcl_platform(platform) windows]} {
	set PLATFORM 1
} elseif {[string match $tcl_platform(platform) unix]} {
	if {[string match $tcl_platform(os) Linux]} {
		set PLATFORM 4
	} else {
		set PLATFORM 3
	}
} elseif {[string match $tcl_platform(platform) macintosh]} {
	set PLATFORM 3
}

if {[catch {main} err]} {
	puts "\[!\] $err"
	#exit
}