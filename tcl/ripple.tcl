set ::images {}
if {![info exists ::after_loading]} {set ::after_loading {}}

proc start_loading {} {
	global images
	global after_loading

	stop_loading

	if {[llength $images] == 0 || [string length [lindex $images 0]] == 0} {
		set images [anigif [file join img ripple-${::IMG_FACTOR}x.gif]]
	}

	.c create text [utils::scaleup 300] [utils::scaleup 300] -text "Loading" -font [list Arial [utils::scale_font 18] bold] -fill #fff -tag loadingtext
	set body "global images; .c delete ripple; .c create image [utils::scaleup 300] [utils::scaleup 200] -image \[utils::lcycle images\] -tag ripple"
	utils::every 30 "::after_loading" $body
}

proc stop_loading {} {
	catch {after cancel $::after_loading}
	catch {.c delete loadingtext ripple}
}

proc anigif file {
	set index 0
	set results {}
	while 1 {
		if [catch {
			image create photo -file "$file" -format "gif -index $index"
		} res] {
			return $results
		}
		lappend results $res
		incr index
	}
}