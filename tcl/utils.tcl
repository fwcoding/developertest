namespace eval utils {
	proc scaleup {n {d 0}} {
		if {$d == 1} {
			return [expr {$n*[tk scaling]}]
		}
		return [expr {round($n*[tk scaling])}]
	}

	proc scale_font {n} {
		if {$::PLATFORM == 3 || [expr {round($::initial_scaling)}] > 1} {
			return [scaleup [expr {$n/$::initial_scaling}]]
		} else {
			return $n
		}
	}

	proc lcycle listName {
		upvar 1 $listName list
		set res [lindex $list 0]
		set list [concat [lrange $list 1 end] [list $res]]
		set res
	}

	proc every {ms id body} {
		global $id
		if {[catch {eval $body}]} {
			# some error happened

			# ^
			# baby jesus cryes everytime you catch and error and don't do anything with it ;_;
		} else {
			set $id [after $ms [info level 0]]
		}
	}
}