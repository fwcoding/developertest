set ::COLOR_LINE_INACTIVE #1F3C4E
set ::COLOR_LINE_ACTIVE #64AAD2
set ::COLOR_BACKGROUND #002236
set ::COLOR_VERSION #13384C
set ::COLOR_BUTTON_HOVER #87CBF1

set ::TEXT_FONT [list Arial [utils::scale_font 14]]
set ::IMG_FACTOR [expr {round([tk scaling])}]


set ::USER {}
set ::PASS {}
set ::LOGIN_STAGE 0


proc paint {} {
	set ::LOGIN_STAGE 0
	lappend ::WIN .c

	tkcon show

	canvas .c -relief flat -bd 0 -takefocus 0 -highlightthickness 0 -bg $::COLOR_BACKGROUND
	pack .c -fill both -expand 1

	image create photo logo -file [file join img fw-logo-${::IMG_FACTOR}x.png]
	.c create image [utils::scaleup 33] [utils::scaleup 40] -anchor w -image logo -tag logo
	.c itemconfig title -text "Log In"

	image create photo user -file [file join img identity-${::IMG_FACTOR}x.png]
	image create photo lock -file [file join img lock-${::IMG_FACTOR}x.png]

	set ::userheight [utils::scaleup 130]
	set ::userx1 [utils::scaleup 33]
	set ::userx2 [utils::scaleup 283]
	set ::usery1 [expr {$::userheight - [utils::scaleup 10]}]
	set ::usery2 [expr {$::userheight + [utils::scaleup 20]}]

	.c create text [utils::scaleup 298] [utils::scaleup -15] -anchor nw -text "7" -font [list {Trebuchet MS} [utils::scale_font 540]] -fill $::COLOR_VERSION -width [utils::scaleup 180] -tag vers
	.c create image [utils::scaleup 43] $::userheight -image user -tag usericon
	.c create line [utils::scaleup 33] [expr {$::userheight + [utils::scaleup 20]}] [utils::scaleup 283] [expr {$::userheight + [utils::scaleup 20]}] -fill $::COLOR_LINE_INACTIVE -tag userline

	lappend ::WIN .user_input
	entry .user_input -relief flat -textvariable ::USER -fg white -insertbackground $::COLOR_LINE_ACTIVE -bg $::COLOR_BACKGROUND -bd 0 -highlightthickness 0 -font $::TEXT_FONT
	place .user_input -x [expr {$::userx1 + [utils::scaleup 30]}] -y $::userheight -anchor w -width [utils::scaleup 220]

	bind .user_input <KeyPress> {
		if {"%K" == "space"} {
			break
		}
	}

	bind .user_input <FocusIn> {
		.c itemconfigure userline -fill $::COLOR_LINE_ACTIVE
	}

	bind .user_input <FocusOut> {
		.c itemconfigure userline -fill $::COLOR_LINE_INACTIVE
	}

	set ::passheight [utils::scaleup 210]
	set ::passx1 [utils::scaleup 33]
	set ::passx2 [utils::scaleup 283]
	set ::passy1 [expr {$::passheight - [utils::scaleup 10]}]
	set ::passy2 [expr {$::passheight + [utils::scaleup 20]}]

	.c create image [utils::scaleup 43] $::passheight -image lock -tag passicon
	.c create line [utils::scaleup 33] [expr {$::passheight + [utils::scaleup 20]}] [utils::scaleup 283] [expr {$::passheight + [utils::scaleup 20]}] -fill $::COLOR_LINE_INACTIVE -tag passline

	lappend ::WIN .pass_input
	entry .pass_input -show * -relief flat -textvariable ::PASS -fg white -insertbackground $::COLOR_LINE_ACTIVE -bg $::COLOR_BACKGROUND -bd 0 -highlightthickness 0 -font $::TEXT_FONT
	place .pass_input -x [expr {$::passx1 + [utils::scaleup 30]}] -y $::passheight -anchor w -width [utils::scaleup 220]

	bind .pass_input <FocusIn> {
		.c itemconfigure passline -fill $::COLOR_LINE_ACTIVE
	}

	bind .pass_input <FocusOut> {
		.c itemconfigure passline -fill $::COLOR_LINE_INACTIVE

	}

	set ::dh [utils::scaleup 36]
	set ::dw [utils::scaleup 250]

	set ::x1 [utils::scaleup 33]
	set ::x2 [expr {$::x1 + $::dw}]
	set ::y1 [utils::scaleup 300]
	set ::y2 [expr {$::y1 + $::dh}]

	.c create rectangle $::x1 $::y1 $::x2 $::y2 -width 0 -fill $::COLOR_LINE_ACTIVE -tag loginbtn
	.c create text [expr {($::x2 - $::x1) / 2 + $::x1}] [expr {($::y2 - $::y1) / 2 + $::y1}] -text "LOGIN" -font $::TEXT_FONT -fill $::COLOR_BACKGROUND -tag logintext

	.c create text [utils::scaleup 300] [utils::scaleup 380] -text "Contacting Server..." -font $::TEXT_FONT -fill #fff -tag motd
	after idle {print_motd}

	bind .c <Motion> {
		set btn_fill [lindex [.c itemconfigure loginbtn -fill] end]
		if {("%x" >= $::x1 && "%x" <= $::x2) && ("%y" >= [expr $::y1 + ($::y2 - $::y1)/2] && "%y" <= $::y2)} {
			if {$btn_fill != "$::COLOR_LINE_INACTIVE" && $btn_fill != "$::COLOR_BUTTON_HOVER"} {
				.c itemconfigure loginbtn -fill $::COLOR_BUTTON_HOVER
			}
		} elseif {$btn_fill != "$::COLOR_LINE_ACTIVE"} {
			.c itemconfigure loginbtn -fill $::COLOR_LINE_ACTIVE
		}
	}

	bind .c <ButtonPress> {
		if {("%x" >= $::x1 && "%x" <= $::x2) && ("%y" >= [expr $::y1 + ($::y2 - $::y1)/2] && "%y" <= $::y2)} {
			.c itemconfigure loginbtn -fill $::COLOR_LINE_INACTIVE
		}
	}

	bind .c <ButtonRelease> {
		if {("%x" >= $::x1 && "%x" <= $::x2) && ("%y" >= [expr $::y1 + ($::y2 - $::y1)/2] && "%y" <= $::y2)} {
			if {$::LOGIN_STAGE == 0} {do_login}
		}

		if {[lindex [.c itemconfigure loginbtn -fill] end] != "$::COLOR_LINE_ACTIVE"} {
			.c itemconfigure loginbtn -fill $::COLOR_LINE_ACTIVE
		}

		if {("%x" >= $::userx1 && "%x" <= $::userx2) && ("%y" >= $::usery1 && "%y" <= $::usery2)} {
			catch {focus .user_input}
		}

		if {("%x" >= $::passx1 && "%x" <= $::passx2) && ("%y" >= $::passy1 && "%y" <= $::passy2)} {
			catch {focus .pass_input}
		}
	}

	bind . <KeyRelease> {
		if {"%K" == "Return" || "%K" == "KP_Enter"} {
			if {$::LOGIN_STAGE == 0} {do_login}
		}
	}

	catch {focus .user_input}
}

proc do_login {} {
	set ::LOGIN_STAGE 1
	
	foreach cel {usericon userline passicon passline loginbtn logintext vers} {
		.c itemconfigure $cel -state hidden
	}

	foreach el {.user_input .pass_input} {
		place forget $el
	}

	.c itemconfigure motd -state hidden
	start_loading

	after 1500 {
		if {[catch {
			set answer [login $::USER $::PASS]
			set is_err [lindex $answer 0]
			set msg [dict get [lindex $answer 1] message]
		}]} {
			set is_err 1
			set msg "Invalid Server Response"
		}

		stop_loading
		if {$is_err} {
			image create photo error -file [file join img error-${::IMG_FACTOR}x.png]
			.c create image [utils::scaleup 300] [utils::scaleup 200] -image error -tag errorimg
			.c create text [utils::scaleup 300] [utils::scaleup 300] -text "$msg" -font [list Arial [utils::scale_font 18]] -fill #fff -tag errortxt
			
			after 2000 {
				set ::LOGIN_STAGE 0

				foreach cel {ripple loadingtext errorimg errortxt} {
					.c itemconfigure $cel -state hidden
				}

				foreach cel {usericon userline passicon passline loginbtn logintext vers motd} {
					.c itemconfigure $cel -state normal
				}

				place .user_input -x [expr {$::userx1 + [utils::scaleup 30]}] -y $::userheight -anchor w -width [utils::scaleup 220]
				place .pass_input -x [expr {$::passx1 + [utils::scaleup 30]}] -y $::passheight -anchor w -width [utils::scaleup 220]
			}
		} else {
			# success login
			image create photo success -file [file join img thumb-${::IMG_FACTOR}x.png]
			.c create image [utils::scaleup 300] [utils::scaleup 200] -image success -tag errorimg
			.c create text [utils::scaleup 300] [utils::scaleup 300] -text "$msg" -font [list Arial [utils::scale_font 18]] -fill #fff -tag errortxt
		}
		
	}
}

proc user_changed {args} {
	if {$::USER == ""} {
		.c itemconfigure usertext -text "Username"
	} else {
		.c itemconfigure usertext -text "$::USER"
	}
}

proc pass_changed {args} {
	if {$::PASS == ""} {
		.c itemconfigure passtext -text "Password"
	} else {
		.c itemconfigure passtext -text "[string repeat * [string length $::PASS]]"
	}
}

proc call_server {method {data {}}} {
	if {[string length $data] > 0} {
		set data [list -query "$data"]
	}

	set token [http::geturl "${::SERVER}${method}" -timeout 5000 {*}$data]
	set code [http::ncode $token]
	set answer [http::data $token]
	http::cleanup $token

	return [list $code $answer]
}

proc server_info {} {
	set answer [call_server /info]
	if {[lindex $answer 0] != 200} {
		return -code error "Invalid Server Response Code"
	}

	if {[catch {
		set schema {dict * string}
		set data [json::json2dict [lindex $answer 1]]
		set data [huddle get_stripped [huddle compile $schema $data]]
	}]} {
		return -code error "Invalid Server Response Body"
	}

	return $data
}

proc print_motd {} {
	if {[catch {
		set motd [dict get [server_info] motd]
		.c itemconfigure motd -text "$motd"
	}]} {
		.c itemconfigure motd -text "Error: Server unreachable."
	}
}

proc login {user pass} {
	set hudd [huddle compile {dict * string} [list username user password pass]]
	set json [huddle jsondump $hudd]
	puts stdout "$json"
	set answer [call_server /login $json]
	puts stdout "$answer"

	if {[catch {
		set res [::json::json2dict [lindex $answer 1]]
	}]} {
		return -code error "Invalid Server Response Body"
	}

	set is_err [expr {[lindex $answer 0] != 200}]

	return [list $is_err $res]
}