[![License: CC BY-NC-ND 4.0](https://img.shields.io/badge/License-CC%20BY--NC--ND%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-nd/4.0/)

# farmerswife test for developers
**Please feel free to do as much of this test as you are comfortable with.**  

![Tcl demo](tcl/demo.gif)

## About the content of this repository:
- **/go**: Simple REST Api used by **/js** and **/tcl** to authenticate and display a "Message of the day".
- **/js**: Simple web login that displays a "Message of the day" and the message "Success!" if the username is "Peter" and password "1".
- **/tcl**: Simple Tcl login that displays a "Message of the day" and the message "Success!" if the username is "Peter" and password "1".

## Requirements:
Before start, check that you have installed the following requirements:

- **ActiveTcl**: https://www.activestate.com/activetcl/downloads
- **Go**: https://golang.org
- **Node.js**: https://nodejs.org
- **Yarn**: https://yarnpkg.com

## Main goal:
- **Fork** this Git repository.

- Fix the following bugs:
	- In **/go**:
		- The server always responds "invalid credentials", even if they are right, please fix this.
	    - **Optional**: Add `goVersion` to the server information response (resource: `/info`) with the currently running version of go. (This can be retrieved programmatically with the [runtime](https://godoc.org/runtime) package)

	- In **/js**:
		- The "Message of the day" is being wrongly truncated. Please, remove the code responsible for this in order to display the whole message.
		
	- In **/tcl**:
		- A member of our support team has reported the following bugs:
			- *The login doesn't work, please fix. Urgent bug.*
			- *The login button doesn't seem to work on it's top half, please fix.*

**When you finish**, *send an email to **coding@farmerswife.com** containing the text **Developer test** in the email subject. Please, feel free to share with us all your thoughts and suggestions about this test.
We will reply to you via email as soon as possible.*

## Instructions

### How to fork
1. Go to [fork this repository](https://bitbucket.org/fwcoding/developertest/fork).
2. Ensure that `This is a private repository` is enabled.
3. Press `Fork repository`.
4. Then go to repository settings > `User and group access`.
5. Add the user `coding@farmerswife.com`.

### How to start coding on the /go folder
1. Open the go folder on the editor of your choice.
1. `cd go/src/server`
1. `go get github.com/gorilla/mux`
1. `go run server.go` (The server should be listening on port 8000)

### How to start coding on the /js folder
1. Open the js folder on the editor of your choice.
1. `cd js`
1. `yarn`
1. `npm start` (This will open your http://localhost:3000)

### How to start coding on the /tcl folder
1. Open the tcl folder on the editor of your choice.
1. `cd tcl`
1. `tclsh init.tcl`

#### This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0)