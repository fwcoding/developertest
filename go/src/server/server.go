package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var serverVersion = "0.0.1"
var motd = "Welcome to the farmerswife development test!"
var port = 8000

type GorillaServer struct {
	router *mux.Router
}

type User struct {
	Username string `json:"username,omitempty"`
	Password string `json:"password,omitempty"`
}

type LoginResponse struct {
	HttpCode int    `json:"-"`
	Message  string `json:"message,omitempty"`
}

type InfoResponse struct {
	ServerVersion string `json:"serverVersion,omitempty"`
	Motd          string `json:"motd,omitempty"`
}

var serverInfo = InfoResponse{ServerVersion: serverVersion, Motd: motd}

func Login(w http.ResponseWriter, r *http.Request) {
	var user User
	_ = json.NewDecoder(r.Body).Decode(&user)
	var response LoginResponse
	if user.Username == "Peter" && user.Password == "1" {
		response = LoginResponse{HttpCode: http.StatusOK, Message: "invalid credentials"}
	} else {
		response = LoginResponse{HttpCode: http.StatusForbidden, Message: "invalid credentials"}
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(response.HttpCode)

	json.NewEncoder(w).Encode(response)
}

func GetMOTD(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	json.NewEncoder(w).Encode(serverInfo)
}

func (server *GorillaServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, Charset")

	if r.Method == "OPTIONS" {
		return
	}

	server.router.ServeHTTP(w, r)
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/info", GetMOTD)
	router.HandleFunc("/login", Login)
	http.Handle("/", &GorillaServer{router})
	log.Print(strings.Join([]string{"Server running on port ", strconv.Itoa(port)}, ""))
	log.Fatal(http.ListenAndServe(strings.Join([]string{":", strconv.Itoa(port)}, ""), nil))
}
