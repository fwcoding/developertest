// Generic react imports
import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import createHistory from 'history/createBrowserHistory'
import { Route, Router } from 'react-router'
import { routerReducer, routerMiddleware } from 'react-router-redux'
import thunk from 'redux-thunk'
import registerServiceWorker from './registerServiceWorker'
import { createMuiTheme, MuiThemeProvider } from 'material-ui/styles'

// app components
import LoginContainer from './containers/LoginContainer'
// app reducers
import loginReducer from './reducers/login'

const history = createHistory()
const middleware = routerMiddleware(history)

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  combineReducers({
    login: loginReducer,
    router: routerReducer
  }),
  composeEnhancers(
    applyMiddleware(middleware, thunk)
  )
)

const muiTheme = createMuiTheme({
  palette: {
    primary: {
      light: '#87CBF1',
      main: '#87CBF1',
      dark: '#67A1C5',
      contrastText: '#002236'
    },
    secondary: {
      main: '#002236'
    },
    type: 'dark',
    background: {
      paper: '#002236'
    },
    divider: 'rgba(60, 102, 126, 0.32)'
  }
})

class App extends Component {
  render () {
    return <MuiThemeProvider theme={muiTheme}>
      <Provider store={store}>
        <Router history={history}>
          <Route exact path='/' component={LoginContainer} />
        </Router>
      </Provider>
    </MuiThemeProvider>
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('root')
)
registerServiceWorker()
