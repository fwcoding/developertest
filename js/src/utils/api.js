import axios from 'axios'
import * as Constants from './constants'
const axiosAPI = axios.create({
    baseURL: Constants.API_URL,
    timeout: 5000,
    headers: {'Accept': 'application/json', 'Content-Type': 'application/json', 'Charset': 'utf-8'}
  })

export function post (path, object) {
  return axiosAPI.post(path, JSON.stringify(object))
}

export function get (path) {
  return axiosAPI.get(path)
}
