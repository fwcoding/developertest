import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Login from '../components/Login'
import * as loginActions from '../actions/login'

function mapStateToProps (state) {
  return {
    login: state.login
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(loginActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
