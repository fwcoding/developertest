import {
  GET_SERVER_INFO_ATTEMPT, GET_SERVER_INFO_ERROR, GET_SERVER_INFO_OK,
  EDIT_LOGIN_FIELD, LOGIN_ATTEMPT, LOGIN_ERROR, LOGIN_OK
} from '../actions/login'

export const initialState = {
  messageOfTheDay: '',
  messageOfTheDayError: '',
  isServerInfoLoading: false,
  isLoginLoading: false,
  username: '',
  password: '',
  loginMessage: ''
}

export default function loginReducer (state = initialState, action) {
  switch (action.type) {
    case GET_SERVER_INFO_ATTEMPT: {
      return Object.assign({}, state, { isServerInfoLoading: true, messageOfTheDayError: '' })
    }
    case GET_SERVER_INFO_ERROR: {
      return Object.assign({}, state, { isServerInfoLoading: false, messageOfTheDayError: action.message })
    }
    case GET_SERVER_INFO_OK: {
      return Object.assign({}, state, { isServerInfoLoading: false, messageOfTheDay: `${action.json.motd.substring(0, 23)}...`, messageOfTheDayError: '' })
    }
    case LOGIN_ATTEMPT: {
      return Object.assign({}, state, { isLoginLoading: true, loginMessage: '' })
    }
    case LOGIN_ERROR: {
      return Object.assign({}, state, { isLoginLoading: false, loginMessage: action.message })
    }
    case LOGIN_OK: {
      return Object.assign({}, state, { isLoginLoading: false, loginMessage: action.json.message })
    }
    case EDIT_LOGIN_FIELD: {
      let mutation = {}
      mutation[action.key] = action.value
      return Object.assign({}, state, mutation)
    }
    default:
      return state
  }
}
