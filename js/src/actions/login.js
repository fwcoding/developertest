import * as ApiUtils from '../utils/api'

export const GET_SERVER_INFO_ATTEMPT = 'GET_SERVER_INFO_ATTEMPT'
export const GET_SERVER_INFO_ERROR = 'GET_SERVER_INFO_ERROR'
export const GET_SERVER_INFO_OK = 'GET_SERVER_INFO_OK'
export const LOGIN_ATTEMPT = 'LOGIN_ATTEMPT'
export const LOGIN_ERROR = 'LOGIN_ERROR'
export const LOGIN_OK = 'LOGIN_OK'
export const EDIT_LOGIN_FIELD = 'EDIT_LOGIN_FIELD'

// getting server info
export function getServerInfoAttempt () {
  return {
    type: GET_SERVER_INFO_ATTEMPT
  }
}

export function getServerInfoError (message) {
  return {
    type: GET_SERVER_INFO_ERROR,
    message
  }
}

export function getServerInfoOk (json) {
  return {
    type: GET_SERVER_INFO_OK,
    json
  }
}

export function getServerInfoRequest () {
  return (dispatch, getState) => {
    dispatch(getServerInfoAttempt())
    ApiUtils.get('/info').then((response) => {
      let json = response.data
      if (json.motd) {
        dispatch(getServerInfoOk(json))
      } else {
        dispatch(getServerInfoError(json.message))
      }
    }).catch((err) => {
      dispatch(getServerInfoError('Error connecting to the server'))
    })
  }
}

// login
export function loginAttempt () {
  return {
    type: LOGIN_ATTEMPT
  }
}

export function loginError (message) {
  return {
    type: LOGIN_ERROR,
    message
  }
}

export function loginOk (json) {
  return {
    type: LOGIN_OK,
    json
  }
}

export function loginRequest () {
  return (dispatch, getState) => {
    dispatch(loginAttempt())
    let body = {
      username: getState().login.username,
      password: getState().login.password
    }
    ApiUtils.post('/login', body).then((response) => {
      let json = response.data
      dispatch(loginOk(json))
    }).catch((err) => {
      if (err.response) {
        if (!!err.response.data.message) {
          dispatch(loginError(`HTTP code ${err.response.status}: ${err.response.data.message}`))
        } else {
          dispatch(loginError('Unknown error'))
        }
      } else {
        dispatch(loginError('Error connecting to the server'))
      }
    })
  }
}

// others
export function changeField (key, value) {
  return { type: EDIT_LOGIN_FIELD, key, value }
}
