import React, { Component } from 'react'
import Typography from 'material-ui/Typography'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import Button from 'material-ui/Button'
import { CircularProgress } from 'material-ui/Progress'
import { withStyles } from 'material-ui/styles'
import Grid from 'material-ui/Grid';
import Card from 'material-ui/Card';

class Login extends Component {
  constructor (props) {
    super(props)
    this.renderMessageOfTheDay = this.renderMessageOfTheDay.bind(this)
    this.renderButtonLogin = this.renderButtonLogin.bind(this)
  }

  componentDidMount () {
    this.props.getServerInfoRequest()
  }

  renderMessageOfTheDay () {
    if (this.props.login.isServerInfoLoading) return <CircularProgress />
    if (this.props.login.messageOfTheDayError !== '') return <Typography>Error: {this.props.login.messageOfTheDayError}</Typography>
    return <Typography>{this.props.login.messageOfTheDay}</Typography>
  }

  renderButtonLogin () {
    if (this.props.login.isLoginLoading) {
      return <Button className={this.props.classes.loginButton} fullWidth variant='raised' color='primary'>
        <CircularProgress style={{color: 'white'}} size={20} /> Loading...
      </Button>
    } else {
      return <Button className={this.props.classes.loginButton} onClick={this.props.loginRequest} fullWidth variant='raised' color='primary'>
        Login
      </Button>
    }
  }
  render () {
    const { classes } = this.props
    return (
      <Grid
        container
        className={classes.fullheight}
        alignItems="center"
        direction="row"
        justify="center"
      >
        <Grid key={1} item>
          <Paper className={classes.container} elevation={5}>
            <Grid
              container
              alignItems="center"
              direction="column"
              justify="center"
            >
              <Grid item xs={12}>
                <img src="fw-logo.png" alt="farmerswife logo" />
              </Grid>
                { this.props.login.loginMessage !== '' ?
                  <Grid item xs={12}>
                    <Card className={classes.card}>
                      <Typography className={classes.loginMessage}>
                        {this.props.login.loginMessage}
                      </Typography>
                    </Card>
                  </Grid> : []}
              <Grid item xs={12}>
                <div className={classes.formContainer}>
                  <TextField fullWidth onChange={(event) => this.props.changeField('username', event.target.value)} label='Username' margin='normal' />
                  <TextField fullWidth onChange={(event) => this.props.changeField('password', event.target.value)} type='password' label='Password' margin='normal' />
                  {this.renderButtonLogin()}
                </div>
              </Grid>
              <Grid item xs={12}>
                <Card className={classes.card}>
                  <Typography className={classes.serverInfo} variant='subheading' color='inherit'>
                    Message of the day:<br />
                    {this.renderMessageOfTheDay()}
                    <Typography>(The message above comes from the Go server)</Typography>
                  </Typography>
                </Card>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

const styles = theme => ({
  card: {
    color: 'white',
    background: 'rgba(255,255,255,0.1)',
    width: '100%'
  },
  fullheight: {
    height: '100%'
  },
  container: {
    margin: 'auto',
    maxWidth: 600,
    padding: 20
  },
  title: {
    textAlign: 'center',
    fontSize: '38pt',
    marginBottom: 20
  },
  subheading: {
    textAlign: 'center'
  },
  formContainer: {
    margin: 'auto',
    width: '80%'
  },
  serverInfo: {
    fontStyle: 'italic',
    borderRadius: 4,
    textAlign: 'center',
    padding: 10
  },
  loginButton: {
    marginTop: 20,
    marginBottom: 20
  },
  loginMessage: {
    fontWeight: 'bold',
    borderRadius: 4,
    textAlign: 'center',
    padding: 10
  }
})

export default withStyles(styles)(Login)
